/**
 * Created by JIMMY on 2/10/2017.
 */
(function () {
    'use strict'
    angular
        .module('app.products')
        .service('ProductService', ProductService);
    function ProductService(TokenRestangular) {
        var service = {
            getProducts: getProducts,
            postProduct: postProduct,
            putProduct: putProduct,
            deleteProduct: deleteProduct,
            getProductDetail: getProductDetail,
            putProductStatus: putProductStatus,
            getProductStockHistory: getProductStockHistory,
            postProductPrice: postProductPrice,
            getProductSalesHistory: getProductSalesHistory,
            getUnit: getUnit

        };
        return service;
        function getProducts(currentPage,filter) {
            debugger;
            return TokenRestangular.all('products?page='+currentPage+ filter).customGET('');
        }
        function getUnit() {
            debugger;
            return TokenRestangular.all('units').customGET('');
        }
        function postProduct(productData) {
            return TokenRestangular.all('product').customPOST(productData);
        }

        function putProduct(product,productId) {
            debugger;
            return TokenRestangular.all('product/'+productId).customPUT(product);
        }
        function putProductStatus(productId,status) {
            return TokenRestangular.all('product/' + productId+'/status/'+status).customPUT('');
        }

        function deleteProduct(productId) {
            return TokenRestangular.all('product/'+productId).customDELETE('');
        }
        function getProductDetail(productId){
            return TokenRestangular.all('product/'+productId+'?include=prices').customGET('');
        }
        function getProductStockHistory(productId){
            return TokenRestangular.all('product/'+productId+'/stockHistory').customGET('');
        }
        function getProductSalesHistory(productId){
            return TokenRestangular.all('product/'+productId+'?include=sales').customGET('');
        }
        function postProductPrice(priceData) {
            return TokenRestangular.all('price').customPOST(priceData);
        }
    }
})();

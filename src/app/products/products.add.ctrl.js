/**
 * Created by JIMMY on 2/10/2017.
 */
(function () {
    'use strict'
    angular
        .module('app.products')
        .controller('ProductAddCtrl', ProductAddCtrl);
    function ProductAddCtrl($scope, $state, $rootScope, $stateParams, CoreService, ProductService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Product Add";
        vm.productData = {};
        vm.units = [];
        vm.postProduct = postProduct;
        vm.updateProduct = updateProduct;
        vm.getUnit = getUnit;
        vm.update = false;
        vm.cancel = cancel;

        if ($stateParams.productId) {
            vm.update = true;
            getProductDetail($stateParams.productId);
        }
        getItemType();
        getUnit();

        function getItemType() {
            debugger;
            CoreService.getTypes().then(function (response) {
                debugger;
                vm.types = response.data;
            });
        }

        function getUnit() {
            debugger;
            ProductService.getUnit().then(function (res) {
                debugger;
                vm.units = res.data;
            });
        }
        function cancel(){
            $state.go('main.products.list');
        }

        function postProduct() {
            debugger;
            CoreService.loadingStart("addProduct");
            vm.disableSubmitButton = true;
            vm.productData.product.price_id = "";
            vm.productData.product.status = "available";
            vm.productData.user = [$rootScope.currentUser.id];

            ProductService.postProduct(vm.productData).then(function (res) {
                vm.productData = {};
                CoreService.loadingStop();
                CoreService.notification('success', "Add Product", "Product Added Successfully");
                $state.go('main.products.list');
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While Adding Product';
                CoreService.notification('error', 'Adding Product', title)
            });

        }

        function getProductDetail(productId) {
            debugger;
            ProductService.getProductDetail(productId).then(function (res) {
                debugger;
                vm.productData.product = res.data.product;
            });

        }

        function updateProduct() {
            debugger;
            CoreService.loadingStart("updateProduct")
            ProductService.putProduct(vm.productData, $stateParams.productId).then(function (res) {
                debugger;
                CoreService.loadingStop();
                CoreService.notification('success', "Product Updated", "Product updated Successfully");
                $state.go('main.products.list');
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While updating  Product';
                CoreService.notification('error', 'Product update', title)
            });
        }

    }
})();

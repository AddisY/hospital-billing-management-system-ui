/**
 * Created by JIMMY on 2/18/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.products')
        .controller('ProductDetailCtrl', ProductDetailCtrl);
    /*@ngNoInject*/
    function ProductDetailCtrl($scope, $state, $rootScope,$stateParams, ProductService, CoreService) {
        var vm = this;
        vm.prices = {};
        vm.product = null;
        vm.stockHistory = [];
        vm.sales = [];
        $rootScope.stateName = "Product Detail";
        vm.getProductDetail = getProductDetail;
        vm.getProductStockHistory = getProductStockHistory;
        vm.getProductSalesHistory = getProductSalesHistory;

        getProductDetail($stateParams.productId);


        function getProductDetail(productId) {
            debugger;
            ProductService.getProductDetail(productId).then(function (res) {
                debugger;
                vm.product = res.data;
                vm.prices = res.data.prices.data;
                getProductStockHistory($stateParams.productId);
            }, function (error) {
                debugger;
                var title = 'Error in Product detail';
                CoreService.notification('error', 'Product Detail', title)
            });

        }

        function getProductStockHistory(productId) {
            debugger;
            ProductService.getProductStockHistory(productId).then(function (res) {
                debugger;
                vm.stockHistory = res;
                getProductSalesHistory($stateParams.productId);
            }, function (error) {
                debugger;
                var title = 'Error in Product Stock History';
                CoreService.notification('error', 'Product Stock History', title)
            });

        }

        function getProductSalesHistory(productId) {
            debugger;
            ProductService.getProductSalesHistory(productId).then(function (res) {
                debugger;
               vm.sales = res.data.sales.data;
            }, function (error) {
                debugger;
                var title = 'Error in Product Sales History';
                CoreService.notification('error', 'Product Sales History', title)
            });

        }
    }
})();

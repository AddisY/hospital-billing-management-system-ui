/**
 * Created by JIMMY on 3/7/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.products')
        .controller('AddProductPriceCtrl', AddProductPriceCtrl);
    /*@ngNoInject*/
    function AddProductPriceCtrl($scope,$rootScope, $state, $stateParams, RolesService, CoreService, ProductService) {
        var vm = this;
        vm.product = null;
        vm.priceData = {};
        vm.addPrice = addPrice;
        vm.getProduct = getProduct;
        vm.cancel = cancel;
        $rootScope.stateName = "Add product price";

        getProduct($stateParams.productId);
        function getProduct(productId) {
            debugger;
            ProductService.getProductDetail(productId).then(function (res) {
                debugger;
                vm.product = res.data;
            }, function (error) {
                debugger;
                var title = 'Error in Product detail';
                CoreService.notification('error', 'Product detail', title)
            });

        }

        function cancel(){
            $state.go('main.products.list');
        }

        function addPrice() {
            debugger;
            CoreService.loadingStart("addProductPrice");
            vm.priceData.type = 'product';
            vm.disableSubmitButton = true;
            vm.priceData.item_id = vm.product.product.id;
            var data = {"price": vm.priceData};
            console.log("s", data);
            ProductService.postProductPrice(data).then(function (res) {
                vm.priceData = {};
                CoreService.loadingStop();
                $state.go('main.products.list');
                CoreService.notification('success', "Add Product Price", "Product Price Added Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While Adding Product Price';
                CoreService.notification('error', 'Adding Product Price', title);
            });
        }
    }

})();



/**
 * Created by JIMMY on 2/8/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.products')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {

        $stateProvider
            .state('main.products', {
                url: "/products",
                templateUrl: "app/products/products.html",
                controller: 'ProductsCtrl',
                controllerAs: 'vm'
            })
            .state('main.products.list', {
                url: "/list",
                templateUrl: "app/products/products.list.html",
                controller: 'ProductsListCtrl',
                controllerAs: 'vm'
            })
            .state('main.products.add',{
                url:"/add",
                templateUrl:"app/products/products.add.html",
                controller:'ProductAddCtrl',
                controllerAs:'vm'
            })
            .state('main.products.detail',{
                url:"/detail/{productId}",
                templateUrl:"app/products/products.detail.html",
                controller:'ProductDetailCtrl',
                controllerAs:'vm'
            })
            .state('main.products.update', {
                url: "/update/{productId}",
                templateUrl: "app/products/products.add.html",
                controller: 'ProductAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.products.addPrice',{
                url:"/addPrice/{productId}",
                templateUrl:"app/products/products.addPrice.html",
                controller:'AddProductPriceCtrl',
                controllerAs:'vm'
            })

    }

})();


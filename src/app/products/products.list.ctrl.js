/**
 * Created by JIMMY on 2/10/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.products')
        .controller('ProductsListCtrl', ProductsListCtrl);
    /*@ngNoInject*/
    function ProductsListCtrl($scope, $state, $rootScope, CoreService, ProductService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Product List";
        vm.products = [];
        vm.itemsPerPage = 12;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.getProducts = getProducts;
        vm.deleteItem = deleteItem;
        vm.updateStatus = updateStatus;
        vm.SearchType = SearchType;

        getProducts();


        function getProducts() {
            debugger;
            vm.products = [];
            ProductService.getProducts(vm.currentPage, vm.filter).then(function (res) {
                debugger;
                vm.products = res.data;
                vm.totalItems = res.meta.pagination.total;
                vm.itemsPerPage = res.meta.pagination.per_page;
            }, function (error) {
                debugger;
                var title = 'Error While listing the Products';
                CoreService.notification('error', 'Listing Products', title)
            });

        }
        function SearchType(filter){
            debugger;
            vm.currentPage = 1;
            vm.filter = filter;
            getProducts();
        }

        function deleteItem(productId) {
            $rootScope.cancel();
            ProductService.deleteProduct(productId).then(function () {
                debugger;
                $state.reload();
                CoreService.notification('success', "Product deleted", "Product deleted Successfully");
            }, function (error) {
                debugger;
                var title = 'Error While deleting the product';
                CoreService.notification('error', 'product Delete', title)
            });
        }

        function updateStatus(productId,status) {
            ProductService.putProductStatus(productId,status).then(function (res) {
                $state.reload();
                CoreService.notification('success', "Product Status Updated", "Product updated Status Successfully");
            }, function (error) {
                debugger;
                var title = 'Error While updating product status ';
                CoreService.notification('error', 'product Status Update', title)
            });
        }
    }

})
();

/**
 * Created by JIMMY on 2/16/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.sales')
        .controller('SalesDetailCtrl', SalesDetailCtrl);
    /*@ngNoInject*/
    function SalesDetailCtrl($scope, $state, $rootScope, $stateParams, CoreService, SalesService) {
        var vm = this;
        vm.salesData = [];
        vm.sales = null;
        vm.price = [];

        vm.getSalesDetail = getSalesDetail;

        getSalesDetail($stateParams.invoice_id);


        function getSalesDetail(invoice_id) {
            debugger;
            SalesService.getSalesDetail(invoice_id).then(function (res) {
                debugger;
                vm.sales = res;
                vm.salesData = res.sales;
                console.log("s", res.sales);
            }, function (error) {
                debugger;
                var title = 'Error in Sales detail';
                CoreService.notification('error', 'Sales detail', title)
            });

        }
    }
})();

/**
 * Created by JIMMY on 2/13/2017.
 */
(function(){
    'use strict'
    angular
        .module('app.sales')
        .controller('SalesListCtrl',SalesListCtrl);
    function SalesListCtrl($scope, $state, $rootScope, SalesService){
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Sales List";
        vm.salesData = {};
        vm.itemsPerPage = 12;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.sales = "";
        vm.getSales = getSales;
        vm.SearchType = SearchType;

        getSales();
        function getSales() {
            debugger;
            console.log("Sales list");
            SalesService.getSales(vm.currentPage, vm.filter).then(function (res) {
                debugger;
                vm.sales = res.data;
                vm.totalItems = res.meta.pagination.total;
                vm.itemsPerPage = res.meta.pagination.per_page;
            });

        }
        function SearchType(filter){
            debugger;
            vm.currentPage = 1;
            vm.filter = filter;
            getSales();
        }

    }
})();

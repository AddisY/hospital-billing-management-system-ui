/**
 * Created by JIMMY on 2/13/2017.
 */
(function(){
    'use strict'
    angular
        .module('app.sales').
        service('SalesService',SalesService);
    function SalesService(TokenRestangular){
        var service = {
            getSales: getSales,
            postSales: postSales,
            getSalesDetail: getSalesDetail

        };
        return service;
        function getSales(currentPage,filter) {
            debugger;
            return TokenRestangular.all('invoices?page='+currentPage+ filter+'?include=sales').customGET('');
        }
        function getSalesDetail(invoice_id) {
            debugger;
            return TokenRestangular.all('invoice/'+invoice_id).customGET('');
        }
        function postSales(sales) {
            debugger;
            return TokenRestangular.all('invoice').customPOST(sales);
        }
    }
})();

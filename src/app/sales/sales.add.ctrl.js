/**
 * Created by JIMMY on 2/13/2017.
 */
(function () {
    'use strict'
    angular
        .module('app.sales')
        .controller('SalesAddCtrl', SalesAddCtrl);
    function SalesAddCtrl($scope, $state, $rootScope, CoreService, $stateParams, SalesService, ProductService, TestServices) {
        var vm = this;
        var types = "product";
        vm.disabled = [];
        vm.disableds =[];
        $rootScope.stateName = "Make Sales";
        vm.salesData = {
            sales: []
        };
        vm.sales = {};
        vm.postSales = postSales;
        vm.getItemType = getItemType;
        vm.userChoice = userChoice;
        vm.getTotalPrice = getTotalPrice;
        vm.getSalesSubTotal = getSalesSubTotal;
        vm.addNew = addNew;
        vm.remove = remove;
        vm.cancel = cancel;
        vm.sales = [{
            quantity: '',
            item_id: '',
            user_id: '',
            type: ''
        }];
        vm.subTotal = [];
        vm.latest_price = [];
        vm.totalPrice = 0;
        vm.salesData.date = moment().format('YYYY-MM-DD');

        getTotalPrice();
        getProducts();

        function addNew(index) {
            debugger;
            if(types =='product'){
                vm.disabled[index]= {status:true};
                debugger;
                var item = {
                    quantity: 0,
                    item_id: vm.products[index].product.type_id,
                    user_id: '',
                    type: vm.products[index].product.type_name,
                    latest_price: vm.products[index].product.latest_price,
                    item : item
                };
                  vm.sales.push(item);
            }
            else if(types  == 'service'){
                debugger;
                vm.disableds[index]= {status:true};
                var item = {
                    quantity: 0,
                    item_id: vm.services[index].id,
                    user_id: '',
                    type: vm.services[index].name,
                    latest_price: vm.services[index].Latest_price,
                    item : item
                };
                vm.sales.push(item);
            }

        }

        function cancel(){
            $state.go('main.sales.list');
        }

        function remove(index) {
            vm.sales.splice(index, 1);
        }

        function getSalesSubTotal(itemId, index) {
            debugger;
            if(types =='product'){
                debugger;
                vm.subTotal[index] = 0;
                for (var i = 0; i < vm.products.length; i++) {
                    if (vm.products[i].product.type_id == itemId) {
                        debugger;
                        vm.latest_price[index] = vm.products[i].product.latest_price;
                        vm.subTotal[index] = (vm.latest_price[index] * vm.sales[index].quantity);
                        debugger;
                        getTotalPrice();
                    }
                }
            }
            else if(types  == 'service'){
                debugger;
                vm.subTotal[index] = 0;
                for (var i = 0; i < vm.services.length; i++) {
                    if (vm.services[i].id == itemId) {
                        debugger;
                        vm.latest_price[index] = vm.services[i].Latest_price;
                        vm.subTotal[index] = (vm.latest_price[index] * vm.sales[index].quantity);
                        debugger;
                        getTotalPrice();

                    }

                }
            }

        }

        function getTotalPrice() {
            debugger;
            vm.totalPrice = 0;
            for (var i = 0; i < vm.sales.length; i++) {
                vm.totalPrice += vm.subTotal[i];
            }
            vm.totalVAT = vm.totalPrice * 0.15;
            vm.grandTotal = vm.totalPrice * 1.15;
        }


        function getItemType() {
            debugger;
            CoreService.getTypes().then(function (response) {
                debugger;
                vm.types = response.data;
            });
        }

        function userChoice(type) {
            debugger;
            if (type == 'service') {
                types = type;
                getServices();
            }
            else if (type == 'product') {
                types = type;
                getProducts();
            }
        }

        function getProducts() {
            debugger;
            if (!vm.products) {
                ProductService.getProducts().then(function (res) {
                    vm.products = res.data;
                });
            }
        }

        function getServices() {
            if (!vm.services) {
                TestServices.getServices().then(function (res) {
                    vm.services = res.data;
                });
            }
        }

        function postSales() {
            CoreService.loadingStart("sales");
            vm.salesData.auto_id = "";
            vm.salesData.address = "";
            vm.disableSubmitButton = true;
            debugger;
            for (var i = 0; i < vm.sales.length; i++) {
                vm.sales[i].user_id = $rootScope.currentUser.id;
            }

            vm.salesData.sales = vm.sales;

            var data = {
                "invoice": vm.salesData
            };
            console.log("s", data);
            SalesService.postSales(data).then(function (res) {
                debugger;
                vm.salesData = {};
                CoreService.loadingStop();
                cancel();
                CoreService.notification('success', "Sales", "Sales Added Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error on add sales';
                CoreService.notification('error', 'sales Added', title)
            });
        }

    }
})();

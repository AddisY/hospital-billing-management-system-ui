/**
 * Created by JIMMY on 2/10/2017.
 */
(function (){
    'use strict'
    angular
        .module('app.sales')
        .config(moduleConfig);
    function moduleConfig($stateProvider){
        $stateProvider
            .state('main.sales',{
                url:"/sales",
                templateUrl:"app/sales/sales.html",
                controller: 'SalesCtrl',
                controllerAs: 'vm'
            })
            .state('main.sales.add',{
                url:"/add",
                templateUrl:"app/sales/sales.add.html",
                controller:'SalesAddCtrl',
                controllerAs:'vm'
            })
            .state('main.sales.list',{
                url:"/list",
                templateUrl:"app/sales/sales.list.html",
                controller:'SalesListCtrl',
                controllerAs:'vm'
            })
            .state('main.sales.detail',{
                url:"/detail/{invoice_id}",
                templateUrl:"app/sales/sales.detail.html",
                controller:'SalesDetailCtrl',
                controllerAs:'vm'
            })

    }

})();

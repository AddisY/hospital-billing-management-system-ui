/**
 * Created by JIMMY on 3/29/2017.
 */

(function () {
    'use strict';

    angular
        .module('app.translation')
        .service('TranslationService', TranslationService);
    /*@ngNoInject*/
    function TranslationService($scope, $resource, TokenRestangular, $rootScope) {

        var service = {
            getTranslation: getTranslation
        };
        return service;
        function getTranslation($resource) {
            debugger;
            this.getTranslation = function ($scope, language) {
                var languageFilePath = 'translation_' + language + '.json';
                $resource(languageFilePath).get(function (data) {
                    $scope.translation = data;
                });
            };

        }
    }

})();



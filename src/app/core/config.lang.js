/**
 * Created by JIMMY on 4/1/2017.
 */
(function() {
    'use strict';

    angular
        .module('app.core')
        .config(translateProvider);
    /*@ngNoInject*/
    function translateProvider($translateProvider) {
       /* $http.get('assets/lang/' + lang + '.json').then(function (response) {
            debugger;
            $rootScope.L = response.data;
        });*/

        $translateProvider.translations('en-US', $http.get('assets/lang/' +'en-US'+ '.json'));
        $translateProvider.translations('am-ET',$http.get('assets/lang/' +'am-ET'+ '.json'));
        $translateProvider.preferredLanguage('en-US');
    }
})();

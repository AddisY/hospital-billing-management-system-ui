/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.core')
        .service('CoreService', CoreService);
    /*@ngNoInject*/
    function CoreService(TokenRestangular, appConstant, $rootScope,notificationService) {
        var loading ="";
        var service = {
            getTypes:getTypes,
            notification:notification,
            loadingStart:loadingStart,
            loadingStop:loadingStop
        };
        return service;

        function getTypes(){
            debugger;
            return TokenRestangular.all('types').customGET('');
        }

        function notification(type, title, text) {
            notificationService.notify({
                title: title,
                title_escape: false,
                text: text,
                text_escape: false,
                styling: "bootstrap3",
                type: type,
                icon: true
            });
        }

        function loadingStart(btnName){
            loading = Ladda.create(document.querySelector('.'+btnName));
            loading.start();
        }
        function loadingStop(){
            loading.stop();
        }

    }

})();

/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.core')
        .controller('CoreCtrl', CoreCtrl);
    /*@ngNoInject*/
    function CoreCtrl($scope, $state, $uibModal, AuthService, $rootScope, $http, $translate) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.currentUser = JSON.parse(localStorage.getItem('user'));
        $rootScope.authService = AuthService;
        $rootScope.logout = logout;
        $rootScope.cancel = cancel;
        $rootScope.confirmationModal = confirmationModal;
        $rootScope.getLanguage = getLanguage;


        function logout() {
            debugger;
            localStorage.removeItem('user');
            localStorage.removeItem('vas_access_token');
            localStorage.removeItem('vas_refresh_token');
            $rootScope.currentUser = localStorage.getItem('user');
            $rootScope.permissions = [];
            $state.go('login');
        }

        function confirmationModal(controllerName, itemId) {
            debugger;
            $scope.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: "app/core/confirmation.modal.html",
                controller: controllerName,
                controllerAs: 'vm',
                size: 'md'
            });
            $rootScope.itemId = itemId;
            $scope.modalInstance.result.then(function (selectedItem) {

            });
        }

        function cancel() {
            $scope.modalInstance.close();

        }

        getLanguage("en-US");

        function getLanguage(lang) {
            debugger;
            // var lang = document.getElementsByTagName('html')[0].getAttribute('lang');
            //var lang = "en-US"; //am-ET
            $http.get('assets/lang/' + lang + '.json').then(function (response) {
                debugger;
                $rootScope.L = response.data;
            });

        }


    }
})();

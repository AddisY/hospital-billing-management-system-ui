(function () {
    'use strict';

    angular
        .module('app.core')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider, $urlRouterProvider, RestangularProvider, notificationServiceProvider) {

        notificationServiceProvider.setDefaults({
            history: false,
            delay: 4000,
            closer: false,
            closer_hover: false
        });

        $urlRouterProvider.otherwise("/main/home");

        RestangularProvider.setErrorInterceptor(
            function (response) {
                if (response.status == 401) {
                    window.location.href = 'index.html#/login';
                }
            }
        );
        $stateProvider
            .state('main', {
                url: "/main",
                templateUrl: "app/core/core.html",
                controller: 'CoreCtrl',
                controllerAs: 'vm'
            });

    }

})();

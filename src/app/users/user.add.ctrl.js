/**
 * Created by JIMMY on 3/3/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.users')
        .controller('UserAddCtrl', UserAddCtrl);
    /*@ngNoInject*/
    function UserAddCtrl($scope, $stateParams, $state, $rootScope, UserService, CoreService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "User Add";
        vm.users = {};
        vm.postUser = postUser;
        vm.cancel = cancel;
        vm.confirmPassword = confirmPassword;
        vm.updateUser = updateUser;
        vm.update = false;
        vm.buttonDisabled = true;
        if ($stateParams.userId) {
            vm.update = true;
            getUserData($stateParams.userId);
        }

        function cancel(){
            $state.go('main.users.list');
        }

        function postUser() {
            debugger;
            CoreService.loadingStart("addUser")
            vm.disableSubmitButton = true;
            var data = {"user": vm.users};
            vm.users.status = "active";
            console.log("s", data);
            UserService.postUser(data).then(function (res) {
                CoreService.loadingStop();
                $state.go('main.users.list');
                CoreService.notification('success', "User created", "User created Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error in User creating';
                CoreService.notification('error', 'User create', title)
            });
        }

        function confirmPassword() {
            if (vm.users.password === vm.password) {
                vm.buttonDisabled = false;
            }
            else {
                vm.buttonDisabled = true;
            }
        }

        function getUserData(userId) {
            UserService.getUsersDetail(userId).then(function (response) {
                debugger;
                vm.users = response.data;
            }, function (error) {
                debugger;
                var title = 'Error While Retrieving user Information';
                CoreService.notification('error', 'Users Detail', title)
            });
        }

        function updateUser() {
            debugger;
            CoreService.loadingStart("updateUser");
            vm.disableSubmitButton = true;
            var data = {
                "user": {
                    name: vm.users.name
                }
            };
            UserService.updateUser($stateParams.userId,data).then(function (res) {
                debugger;
                CoreService.notification('success', "User updated", "User updated Successfully");
                CoreService.loadingStop();
                $state.go('main.users.list');
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While updating user';
                CoreService.notification('error', 'User update', title)
            });
        }

    }
})
();


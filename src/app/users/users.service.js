/**
 * Created by JIMMY on 3/3/2017.
 */
(function () {
    'use strict'
    angular
        .module('app.users')
        .service('UserService', UserService);
    function UserService(TokenRestangular) {
        var service = {
            getUsers: getUsers,
            postUser: postUser,
            getUsersDetail: getUsersDetail,
            putProductStatus: putProductStatus,
            updateUser: updateUser

        };
        return service;
        function getUsers(currentPage, filter) {
            debugger;
            return TokenRestangular.all('users?page='+currentPage+ filter).customGET('');
        }

        function getUsersDetail(userId) {
            debugger;
            return TokenRestangular.all('users/' + userId).customGET('');
        }

        function postUser(typeData) {
            debugger;
            return TokenRestangular.all('users').customPOST(typeData);
        }

        function putProductStatus(userId, status) {
            return TokenRestangular.all('users/' + userId + '/status/' + status).customPUT('');
        }

        function updateUser(userId, user) {
            debugger;
            return TokenRestangular.all('users/' + userId).customPUT(user);
        }
    }
})();
/**
 * Created by JIMMY on 3/3/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.users')
        .controller('UserDetailCtrl', UserDetailCtrl);
    /*@ngNoInject*/
    function UserDetailCtrl($scope, $stateParams, $state,HomeService, $rootScope, UserService, CoreService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "User Detail";

        getUserDetail($stateParams.userId);
        getSalesReport($stateParams.userId);

        function getUserDetail(userId) {
            UserService.getUsersDetail(userId).then(function (response) {
                vm.user = response.data;
            }, function (error) {
                debugger;
                var title = 'Error While Retrieving user Information';
                CoreService.notification('error', 'Users Detail', title)
            });
        }

        function getSalesReport(userId) {
            debugger;
            HomeService.userSales(userId).then(function (response) {
                vm.sales = response.data;
            }, function (error) {
                debugger;
                var title = 'Error While Retrieving user Sales';
                CoreService.notification('error', 'Users Sales Report', title)
            });
        }

    }
})
();


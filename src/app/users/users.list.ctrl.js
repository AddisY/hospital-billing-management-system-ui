/**
 * Created by JIMMY on 3/3/2017.
 */

(function () {
    'use strict';

    angular
        .module('app.users')
        .controller('UsersListCtrl', UsersListCtrl);
    /*@ngInject*/
    function UsersListCtrl($state,$rootScope, $scope, UserService, CoreService) {

        var vm = this;
        $rootScope.stateName = "User List";
        vm.users = [];
        vm.getUsers = getUsers;
        vm.updateStatus = updateStatus;
        vm.deleteItem = deleteItem;
        vm.SearchType = SearchType;


        getUsers();

        function getUsers() {
            debugger;
            UserService.getUsers().then(function (res) {
                debugger;
                vm.users = res.data;
            }, function (error) {
                debugger;
                var title = 'Error While Retrieving user';
                CoreService.notification('error', 'Retrieving user', title)
            });

        }
        function SearchType(filter){
            debugger;
            vm.currentPage = 1;
            vm.filter = filter;
            getUsers();
        }

        function updateStatus(userId, status) {
            UserService.putProductStatus(userId, status).then(function (response) {
                debugger;
                $state.reload();
                var title = 'Successfully Updated Status';
                CoreService.notification('success', 'Status Update', title)
            }, function (error) {
                debugger;
                var title = 'Error While Updating Status';
                CoreService.notification('error', 'Status Update', title)
            });

        }

        function deleteItem(userId) {
            $rootScope.cancel();
            var title = 'Feature Not Available';
            CoreService.notification('warning', 'User Delete', title)
        }


    }

})();

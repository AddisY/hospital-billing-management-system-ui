/**
 * Created by JIMMY on 3/3/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.users')
        .controller('UsersCtrl', UsersCtrl);
    /*@ngNoInject*/
    function UsersCtrl($scope, $state, $rootScope) {
        var vm = this;
        $rootScope.currentState = $state.current.name;

        $rootScope.stateName = "Users";

    }
})();

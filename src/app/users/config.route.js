/**
 * Created by JIMMY on 3/3/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.users')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {

        $stateProvider
            .state('main.users', {
                url: "/users",
                templateUrl: "app/users/users.html",
                controller: 'UsersCtrl',
                controllerAs: 'vm'
            })
            .state('main.users.list', {
                url: "/list",
                templateUrl: "app/users/users.list.html",
                controller: 'UsersListCtrl',
                controllerAs: 'vm'
            })
            .state('main.users.add', {
                url: "/add",
                templateUrl: "app/users/user.add.html",
                controller: 'UserAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.users.detail', {
                url: "/detail/{userId}",
                templateUrl: "app/users/user.detail.html",
                controller: 'UserDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.users.assignRole', {
                url: "/{userId}/assignRole",
                templateUrl: "app/roles/role_assign.html",
                controller: 'RoleAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.users.update', {
                url: "/update/{userId}",
                templateUrl: "app/users/user.add.html",
                controller: 'UserAddCtrl',
                controllerAs: 'vm'
            })
    }

})();




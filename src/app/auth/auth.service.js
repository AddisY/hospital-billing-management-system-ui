/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.auth')
        .service('AuthService', AuthService);
    /*@ngNoInject*/
    function AuthService(TokenRestangular, appConstant, $rootScope) {
        var service = {
            getLoggedInUser: getLoggedInUser,
            login: login,
            getUserPermissions: getUserPermissions,
            checkPermission: checkPermission

        };

        return service;

        function login(data) {
            debugger;
            return TokenRestangular.all('oauth/authorize').customPOST(data);

        }

        function getLoggedInUser() {
            debugger;
            return TokenRestangular.all('user?include=roles').customGET('');
        }

        function getUserPermissions(currentUserStr) {
            var currentUser = JSON.parse(currentUserStr);

            var perms = [];

            if (currentUser) {
                for (var i = 0; i < currentUser.roles.data.length; i++) {
                    for (var j = 0; j < currentUser.roles.data[i].perms.data.length; j++) {
                        perms.push(currentUser.roles.data[i].perms.data[j].name);
                    }
                }
            }

            return perms;
        }

        function checkPermission(permission) {

            var perms = getUserPermissions(localStorage.getItem('user'));

            for (var i = 0; i < perms.length; i++) {
                if (permission === perms[i]) {
                    return true;
                }
            }

            return false;
        }


    }

})
();

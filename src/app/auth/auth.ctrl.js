/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('AuthCtrl', AuthCtrl);
    /*@ngNoInject*/
    function AuthCtrl($scope, $state, $rootScope, TokenRestangular, AuthService, appConstant) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        vm.email = '';
        vm.password = '';
        vm.passwordConfirmation = '';
        vm.loginError = '';
        vm.authenticating = false;
        vm.login = login;
        vm.register = register;
        vm.redirect_state = "main.home";
        $rootScope.logout = logout;

        function login() {
            vm.authenticating = true;
            vm.loginError = '';
            var data = {
                "grant_type": appConstant.grant_type,
                "client_id": appConstant.client_id,
                "client_secret": appConstant.client_secret,
                "username": vm.email,
                "password": vm.password
            };

            debugger;
            AuthService.login(data).then(function (response) {
                debugger;
                localStorage.setItem('vas_access_token', response.access_token);
                localStorage.setItem('vas_refresh_token', response.refresh_token);
                TokenRestangular.setDefaultHeaders({Authorization: 'Bearer ' + localStorage.getItem('vas_access_token')});

                AuthService.getLoggedInUser().then(function (response) {
                    debugger;
                    var user = JSON.stringify(response.data);
                    localStorage.setItem('user', user);
                    $rootScope.currentUser = response.data;
                    $state.go(vm.redirect_state);
                });
            }, function (error) {
                debugger;
                if (error.statusText == 'Unauthorized') {
                    vm.loginError = "Invalid username or password !";
                    vm.password = '';
                }
                else {
                    vm.loginError = error.statusText ? error.statusText : 'Please Check Your Network Connection!';
                    vm.password = '';
                }
                vm.authenticating = false;
            })

        }

        function register() {
            if (vm.password != vm.passwordConfirmation) {
                vm.loginError = "Passwords doesnt match";
                vm.password = '';
                vm.passwordConfirmation = '';
            }
            else {
                var data = {
                    "name": vm.name,
                    "username": vm.email,
                    "password": vm.password
                };
                AuthService.register(data).then(function (response) {
                    login();
                });
            }

        }

        function logout() {
            debugger;
            localStorage.removeItem('user');
            localStorage.removeItem('vas_access_token');
            localStorage.removeItem('vas_refresh_token');
            $rootScope.currentUser = null;
            $rootScope.permissions = [];
            $state.go('login');
        }
    }
})();

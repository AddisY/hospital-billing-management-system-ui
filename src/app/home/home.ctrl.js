/**
 * Created by acer1 on 10/26/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.home')
        .controller('HomeCtrl', HomeCtrl);
    /*@ngNoInject*/
    function HomeCtrl($scope, $state, $rootScope, $uibModal, AuthService, SalesService, HomeService, CoreService) {
        var vm = this;
        $rootScope.l = "world wide";
        $rootScope.currentState = $state.current.name;
        vm.dashboard = "";
        vm.pieChart = {
            chart: {
                type: 'pieChart',
                height: 350,
                x: function (d) {
                    return d.key;
                },
                y: function (d) {
                    return d.y;
                },
                showLabels: true,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                }
            }
        };
        vm.dashboardCalender_1 = ({
            date: {
                startDate: moment().subtract(1, 'months'),
                endDate: moment()
            }
        });
        vm.getDashbord = getDashboard;
        vm.getUsersSalesGraph = getUsersSalesGraph;

        getDashboard();

        if (AuthService.checkPermission('admin-dashboard')) {
            getUsersSalesGraph(vm.dashboardCalender_1, 'Day');
        }
        else{
            getSalesReport($rootScope.currentUser.id);
        }

        function getDashboard() {
            debugger;
            HomeService.getDashboard().then(function (res) {
                debugger;
                vm.dashboard = res.data;
                vm.countGraph = [
                    {
                        key: "Products",
                        y: res.data.totalProduct
                    },
                    {
                        key: "Services",
                        y: res.data.totalService
                    }
                ];
                vm.profitGraph = [
                    {
                        key: "Products",
                        y: res.data.totalProductSalesPrice
                    },
                    {
                        key: "Services",
                        y: res.data.totalServiceSalesPrice
                    }
                ];
            }, function (error) {
                debugger;
                var title = 'Error While Creating the Article';
                CoreService.notification('error', 'Article Create', title)
            });
        }

        function getUsersSalesGraph(dateRange, graphType) {
            debugger;
            var data = {
                "dateTimeRange": {
                    from: changeToDateTime(dateRange.startDate),
                    to: changeToDateTime(dateRange.endDate),
                    "type": graphType
                }
            };
            vm.dates = [];
            vm.users = [];
            vm.salesData = [];
            HomeService.getUsersSalesData(data, graphType).then(function (response) {
                vm.usersSalesData = response;
                vm.salesData = create2DArray(vm.usersSalesData.length, vm.usersSalesData[0].users.length);
                for (var i = 0; i < vm.usersSalesData.length; i++) {
                    vm.dates.push(vm.usersSalesData[i].date);
                    for (var x = 0; x < vm.usersSalesData[i].users.length; x++) {
                        vm.users.push(vm.usersSalesData[i].users[x].name);
                        vm.salesData[i][x] = vm.usersSalesData[i].users[x].value;
                    }
                }

            });
        }

        function changeToDateTime(dateTime) {
            return moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
        }

        function create2DArray(rows, columns) {
            var x = new Array(rows);
            for (var i = 0; i < rows; i++) {
                x[i] = new Array(columns);
            }
            return x;
        }

        function getSalesReport(userId) {
            debugger;
            HomeService.userSales(userId).then(function (response) {
                vm.sales = response.data;
            }, function (error) {
                debugger;
                var title = 'Error While Retrieving user Sales';
                CoreService.notification('error', 'Users Sales Report', title)
            });
        }


    }
})();

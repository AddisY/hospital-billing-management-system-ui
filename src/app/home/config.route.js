(function () {
    'use strict';

    angular
        .module('app.home')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {

        $stateProvider
            .state('main.home', {
                url: "/home",
                templateUrl: "app/home/home.html",
                controller: 'HomeCtrl',
                controllerAs: 'vm'
            })
            .state('main.contactUs', {
                url: "/contactUs",
                templateUrl: "app/home/contactUs.html",
                controller: '',
                controllerAs: 'vm'
            })
            .state('main.aboutUs', {
                url: "/aboutUs",
                templateUrl: "app/home/aboutUs.html",
                controller: 'AboutUsCtrl',
                controllerAs: 'vm'
            })

    }

})();

/**
 * Created by Nejib on 12/24/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.home')
        .service('HomeService', HomeService);
    /*@ngNoInject*/
    function HomeService(TokenRestangular, $rootScope) {

        var service = {
            getDashboard: getDashboard,
            getUsersSalesData: getUsersSalesData,
            userSales:userSales
        };

        return service;

        function getUsersSalesData(data, filterType) {
            return TokenRestangular.all('dashboard?type=' + filterType).customPOST(data);
        }

        function getDashboard() {
            return TokenRestangular.all('dashboard').customGET('');
        }

        function userSales(userId) {
            return TokenRestangular.all('user/' + userId + '/sales').customGET('');
        }


    }

})();


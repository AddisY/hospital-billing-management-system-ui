/**
 * Created by JIMMY on 3/8/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .controller('AddServicePriceCtrl', AddServicePriceCtrl);
    /*@ngNoInject*/
    function AddServicePriceCtrl($scope,$rootScope, $state, $stateParams, RolesService, CoreService, TestServices) {
        var vm = this;
        vm.service = null;
        vm.priceData = {};
        vm.addPrice = addPrice;
        vm.getService = getService;
        vm.cancel = cancel;
        $rootScope.stateName = "Add Service price";

        getService($stateParams.serviceId);

        function getService(serviceId) {
            debugger;
            TestServices.getServicesDetail(serviceId).then(function (res) {
                debugger;
                vm.service = res.data;
            }, function (error) {
                debugger;
                var title = 'Error in Product Sales';
                CoreService.notification('error', ' Product Sales', title)
            });

        }

        function cancel(){
            $state.go('main.services.list');
        }

        function addPrice() {
            debugger;
            CoreService.loadingStart("addPrice");
            vm.disableSubmitButton = true;
            vm.priceData.type = 'service';
            vm.priceData.item_id = vm.service.id;
            var data = {"price": vm.priceData};
            console.log("s", data);
            TestServices.postServicePrice(data).then(function (res) {
                vm.priceData = {};
                CoreService.loadingStop();
                $state.go('main.services.list');
                CoreService.notification('success', "Service Add Price", "Service Price Added Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error in Adding Service Price';
                CoreService.notification('error', 'Adding Service Price', title)
            });
        }
    }

})();



(function () {
    'use strict';

    angular
        .module('app.services')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {

        $stateProvider
            .state('main.services', {
                url: "/services",
                templateUrl: "app/services/services.html",
                controller: 'ServicesCtrl',
                controllerAs: 'vm'
            })
            .state('main.services.list', {
                url: "/list",
                templateUrl: "app/services/services.list.html",
                controller: 'ServicesListCtrl',
                controllerAs: 'vm'
            })
            .state('main.services.add', {
                url: "/add",
                templateUrl: "app/services/services.add.html",
                controller: 'ServicesAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.services.detail', {
                url: "/detail/{serviceId}",
                templateUrl: "app/services/services.detail.html",
                controller: 'ServiceDetailCtrl',
                controllerAs: 'vm'
            })
            .state('main.services.update', {
                url: "/update/{serviceId}",
                templateUrl: "app/services/services.add.html",
                controller: 'ServicesAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.services.addPrice', {
                url: "/addPrice/{serviceId}",
                templateUrl: "app/services/services.addPrice.html",
                controller: 'AddServicePriceCtrl',
                controllerAs: 'vm'
            })

    }

})();

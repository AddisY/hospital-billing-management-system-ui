/**
 * Created by JIMMY on 2/8/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .controller('ServicesAddCtrl', ServicesAddCtrl);
    /*@ngNoInject*/
    function ServicesAddCtrl($scope, $state, $rootScope, $stateParams, TestServices, CoreService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Service Add";
        vm.serviceData = {};
        vm.postServices = postServices;
        vm.updateService = updateService;
        vm.getServiceType = getServiceType;
        vm.cancel = cancel;
        vm.update = false;

        getServiceType();

        if ($stateParams.serviceId) {
            vm.update = true;
            getServicesDetail($stateParams.serviceId);
        }
        getServiceType();

        function getServiceType() {
            vm.services = [];
            TestServices.getServicesType().then(function (res) {
                vm.services = res.data;
            });
        }

        function postServices() {
            debugger;
            CoreService.loadingStart("addService");
            vm.disableSubmitButton = true;
            var data = {"service": vm.serviceData};
            vm.serviceData.price_id = "";
            vm.serviceData.status = "available";
            console.log("s", data);
            TestServices.postServices(data).then(function (res) {
                vm.serviceData = {};
                CoreService.loadingStop();
                $state.go('main.services.list');
                CoreService.notification('success', "Add Service", "Service Added Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error in Adding Service';
                CoreService.notification('error', 'Adding Service', title)
            });

        }
        function cancel(){
            $state.go('main.services.list');
        }

        function getServicesDetail(serviceId) {
            debugger;
            TestServices.getServicesDetail(serviceId).then(function (res) {
                debugger;
                vm.serviceData = res.data;
            });

        }


        function updateService() {
            debugger;
            CoreService.loadingStart("updateService");
            vm.disableSubmitButton = true;
            var data = {"service": vm.serviceData};
            TestServices.putService(data, $stateParams.serviceId).then(function (res) {
                debugger;
                CoreService.loadingStop();
                $state.go('main.services.list');
                CoreService.notification('success', "Service Update", "Service updated Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While updating  service';
                CoreService.notification('error', 'Service update', title);
            });
        }


    }
})
();

/**
 * Created by Nejib on 12/24/2016.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .controller('ServicesCtrl', ServicesCtrl);
    /*@ngNoInject*/
    function ServicesCtrl($scope, $state, $rootScope) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Service";

    }
})();


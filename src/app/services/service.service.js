/**
 * Created by JIMMY on 2/10/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .service('TestServices', TestServices);
    /*@ngNoInject*/
    function TestServices(TokenRestangular) {

        var service = {
            getServices: getServices,
            postServices: postServices,
            putService: putService,
            deleteService: deleteService,
            getServicesDetail: getServicesDetail,
            putServiceStatus: putServiceStatus,
            getServicesType: getServicesType,
            postServicePrice: postServicePrice

        };
        return service;
        function getServices(currentPage, filter) {
            return TokenRestangular.all('services?page='+currentPage+ filter).customGET('');
        }

        function getServicesType() {
            return TokenRestangular.all('service_types_dropdown').customGET('');
        }

        function getServicesDetail(serviceId) {
            return TokenRestangular.all('service/' + serviceId).customGET('');
        }


        function postServices(serviceData) {
            debugger;
            return TokenRestangular.all('service').customPOST(serviceData);
        }

        function postServicePrice(priceData) {
            debugger;
            return TokenRestangular.all('price').customPOST(priceData);
        }

        function putService(service,serviceId) {
            debugger;
            return TokenRestangular.all('service/' + serviceId).customPUT(service);
        }

        function putServiceStatus(serviceId,status) {
            debugger;
            return TokenRestangular.all('service/' + serviceId + '/status/'+status).customPUT('');
        }

        function deleteService(serviceId) {
            debugger;
            return TokenRestangular.all('service/' + serviceId).customDELETE('');
        }


    }

})();

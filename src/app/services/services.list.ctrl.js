/**
 * Created by JIMMY on 2/9/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .controller('ServicesListCtrl', ServicesListCtrl);
    /*@ngNoInject*/
    function ServicesListCtrl($scope, $state, $rootScope, TestServices, CoreService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Service List";
        vm.itemsPerPage = 12;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.serviceData = {
            "service": {},
            "prerequisites": []
        };
        vm.services = [];
        vm.getServices = getServices;
        vm.deleteItem = deleteItem;
        vm.updateStatus = updateStatus;
        vm.SearchType = SearchType;

        getServices();

        function getServices() {
            debugger;
            vm.services = [];
            TestServices.getServices(vm.currentPage, vm.filter).then(function (res) {
                debugger;
                vm.services = res.data;
                vm.totalItems = res.meta.pagination.total;
                vm.itemsPerPage = res.meta.pagination.per_page;
            }, function (error) {
                debugger;
                var title = 'Error in Listing Services';
                CoreService.notification('error', 'Service List', title);
            });

        }
        function SearchType(filter){
            debugger;
            vm.currentPage = 1;
            vm.filter = filter;
            getServices();
        }

        function deleteItem(serviceId) {
            debugger;
            $rootScope.cancel();
            TestServices.deleteService(serviceId).then(function () {
                $state.reload('main.services.list');
                CoreService.notification('success', "Service delete", "Service deleted Successfully");
            }, function (error) {
                debugger;
                var title = 'Error While deleting the service';
                CoreService.notification('error', 'Service Delete', title)
            });
        }
        function updateStatus(serviceId,status) {
            debugger;
            TestServices.putServiceStatus(serviceId,status).then(function (res) {
                $state.reload();
                CoreService.notification('success', "Service Status Updated", "Service Status updated Successfully");
            }, function (error) {
                debugger;
                var title = 'Error While updating  service';
                CoreService.notification('error', 'Service Status update', title);
            });
        }

    }
})();

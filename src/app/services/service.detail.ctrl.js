    /**
     * Created by JIMMY on 2/17/2017.
     */
    (function () {
        'use strict';

        angular
            .module('app.services')
            .controller('ServiceDetailCtrl', ServiceDetailCtrl);
        /*@ngNoInject*/
        function ServiceDetailCtrl($scope, $state, $rootScope, $stateParams, TestServices, CoreService) {
            var vm = this;
            $rootScope.currentState = $state.current.name;
            $rootScope.stateName = "Service Detail";
            vm.service = "";
            vm.price = {};
            vm.sales = {};
            vm.serviceData = {};
            vm.getServicesDetail = getServicesDetail;

            getServicesDetail($stateParams.serviceId);

            function getServicesDetail(serviceId) {
                debugger;
                TestServices.getServicesDetail(serviceId).then(function (res) {
                    debugger;
                    vm.service = res.data;
                }, function (error) {
                    debugger;
                    var title = 'Error in Service Detail';
                    CoreService.notification('error', 'Service Detail', title)
                });

            }

        }
    })();

(function () {
    'use strict';

    angular.module('app', ['app.core', 'app.auth', 'app.home','app.services','app.products','app.sales','app.configuration','app.users','app.roles'])
        .constant("appConstant", {
            "localApi": "http://localhost:8000/api/",
            "imagePath": "http://localhost:8000/",
            "grant_type": "password",
            "client_id": "$2y$10$jvw/V6Fo9mvp4JXDCYAB..uYpTEl27ZCD",
            "client_secret": "$2y$10$9OqJjxC9qZKAB2L.nO7hVOPY0436eU1CD"
        })
        .factory("TokenRestangular", tokenRestangular);


    /*@ngNoInject*/
    function tokenRestangular(Restangular, appConstant) {
        /*@ngNoInject*/
        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setDefaultHeaders({Authorization: 'Bearer ' + localStorage.getItem('vas_access_token')});
            RestangularConfigurer.setBaseUrl(appConstant.localApi);
        });

    }
})();


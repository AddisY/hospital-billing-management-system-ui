/**
 * Created by JIMMY on 3/6/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.roles')
        .controller('RolesListCtrl', RolesListCtrl);
    /*@ngNoInject*/
    function RolesListCtrl($state, $scope, $rootScope, RolesService, CoreService) {
        var vm = this;
        vm.roles = {};

        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Roles List"
        vm.itemsPerPage = 12;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.SearchType = SearchType;

        vm.getRoles = getRoles;

        getRoles();

        function getRoles() {
            RolesService.getRoles(vm.currentPage, vm.filter).then(function (res){
                debugger;
                vm.roles = res.data
                vm.totalItems = res.meta.pagination.total;
                vm.itemsPerPage = res.meta.pagination.per_page;
            }, function (error) {
                debugger;
                var title = 'Error While Listing the roles';
                CoreService.notification('error', 'Listing Roles', title)
            });
        }
        function SearchType(filter){
            debugger;
            vm.currentPage = 1;
            vm.filter = filter;
            getRoles();
        }

    }

})();


/**
 * Created by JIMMY on 3/6/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.roles')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {
        $stateProvider
            .state('main.roles', {
                url: "/roles",
                templateUrl: "app/roles/roles.html",
                controllerAs: 'vm'
            })
            .state('main.roles.list', {
                url: "/list",
                templateUrl: "app/roles/roles.list.html",
                controller: 'RolesListCtrl',
                controllerAs: 'vm'
            })
            .state('main.roles.add', {
                url: "/add",
                templateUrl: "app/roles/role.add.html",
                controller: 'RoleAddCtrl',
                controllerAs: 'vm'
            })
            .state('main.roles.update', {
                url: "/update/{roleId}",
                templateUrl: "app/roles/role.add.html",
                controller: 'RoleAddCtrl',
                controllerAs: 'vm'
            })

    }
})();
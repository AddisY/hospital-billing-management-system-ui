/**
 * Created by JIMMY on 3/6/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.roles')
        .service('RolesService', RolesService);
    /*@ngNoInject*/
    function RolesService(TokenRestangular) {
        var service = {
            getRoles: getRoles,
            assignRoles: assignRoles,
            getPermissions: getPermissions,
            postRole: postRole,
            postPermissions: postPermissions,
            updateRole: updateRole,
            deleteRole: deleteRole,
            getRoleDetail: getRoleDetail
        };

        return service;
        function getRoles(currentPage,filter) {
            return TokenRestangular.all('roles?page='+currentPage+ filter).customGET('');
        }

        function assignRoles(userId, roleData) {
            debugger;
            return TokenRestangular.all('users/' + userId + '/user' + '/roles').customPOST(roleData);
        }

        function getPermissions() {
            return TokenRestangular.all('permissions').customGET('');
        }

        function postRole(roleData) {
            debugger;
            return TokenRestangular.all('role').customPOST(roleData);
        }

        function postPermissions(roleId, permission) {
            debugger;
            return TokenRestangular.all('roles/' + roleId + '/permissions').customPOST(permission);
        }

        function updateRole(role, roleId) {
            debugger;
            return TokenRestangular.all('role/' + roleId).customPUT(role);
        }

        function deleteRole() {
            debugger;
            return TokenRestangular.all().customDELETE();
        }

        function getRoleDetail(roleId) {
            debugger;
            return TokenRestangular.all('role/' + roleId).customGET();
        }
    }

})();

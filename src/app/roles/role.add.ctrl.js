/**
 * Created by JIMMY on 3/6/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.roles')
        .controller('RoleAddCtrl', RoleAddCtrl);
    /*@ngNoInject*/
    function RoleAddCtrl($scope,$rootScope, $state, $stateParams, RolesService, CoreService, UserService) {
        var vm = this;
        vm.roles = [];
        vm.user = null;
        vm.rolesData = [];
        vm.permission = [];
        vm.role = {};
        vm.getRoles = getRoles;
        vm.assignRoles = assignRoles;
        vm.getRoles = getRoles;
        vm.getUser = getUser;
        vm.getPermissions = getPermissions;
        vm.postRole = postRole;
        vm.updateRole = updateRole;
        vm.deleteRole = deleteRole;
        vm.cancel = cancel;
        vm.roleUpdate = false;

        $rootScope.stateName = "Add Roles";
        getRoles();
        getPermissions();

        if ($stateParams.userId) {
            getUser($stateParams.userId);
        }
        if ($stateParams.roleId) {
            getRoleDetail($stateParams.roleId);
            vm.roleUpdate = true;
        }

        function getRoleDetail(roleId) {
            debugger;
            RolesService.getRoleDetail(roleId).then(function (res) {
                debugger;
                vm.role = res.data;
                for (var i = 0; i < vm.role.perms.data.length; i++) {
                    vm.permission.push(vm.role.perms.data[i]);
                    vm.permission[i].isChecked = true
                }
            }, function (error) {
                debugger;
                var title = 'Error While retrieving the role';
                CoreService.notification('error', 'Role Listing', title)
            });
        }

        function getRoles() {
            RolesService.getRoles().then(function (res) {
                debugger;
                vm.roles = res.data;
            }, function (error) {
                debugger;
                var title = 'Error While Listing the roles';
                CoreService.notification('error', 'Role Listing', title)
            });
        }

        function cancel() {
            $state.go('main.roles.list');
        }

        function getUser(userId) {
            debugger;
            UserService.getUsersDetail(userId).then(function (res) {
                vm.user = res.data;
            }, function (error) {
                debugger;
                var title = 'Error while assign role';
                CoreService.notification('error', 'assign role', title)
            });

        }

        function getPermissions() {
            RolesService.getPermissions().then(function (res) {
                debugger;
                vm.permission = res.data;
            }, function (error) {
                debugger;
                var title = 'Error While Listing the Permissions';
                CoreService.notification('error', 'Permissions Listing', title)
            });
        }

        function assignRoles() {
            debugger;
            CoreService.loadingStart("assignRole");
            vm.disableSubmitButton = true;
            var rolesData = [];
            for (var i = 0; i < vm.roles.length; i++) {
                if (vm.roles[i].isChecked) {
                    rolesData.push(vm.roles[i].id);
                }
            }
            vm.rolesData = {"role_id_list": rolesData};
            RolesService.assignRoles(vm.user.id, vm.rolesData).then(function (res) {
                CoreService.loadingStop();
                $state.go('main.users.list');
                CoreService.notification('success', "Assign Roles", "Assign Roles Successfully");
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error while assign role';
                CoreService.notification('error', 'assign role', title)
            });

        }

        function postRole() {
            debugger;
            CoreService.loadingStart("addRole");
            vm.disableSubmitButton = true;
            vm.rolesData = {"role": vm.role};
            RolesService.postRole(vm.rolesData).then(function (res) {
                vm.rolesData = {};
                var roleId = res.data.id;
                debugger;
                var permission = [];
                for (var i = 0; i < vm.permission.length; i++) {
                    if (vm.permission[i].isChecked) {
                        permission.push(vm.permission[i].id);
                    }
                }
                vm.permission = {"permission_id_list": permission};
                RolesService.postPermissions(roleId, vm.permission).then(function (res) {
                    CoreService.loadingStop();
                    $state.go('main.roles.list');
                    CoreService.notification('success', "Assign Permission", "Assign Permission Successfully");
                }, function (error) {
                    debugger;
                    CoreService.loadingStop();
                    var title = 'Error while assign permission';
                    CoreService.notification('error', 'assign permission', title)
                });
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While Assign Permission';
                CoreService.notification('error', 'Assign Permission', title)
            });
        }

        function updateRole(roleId) {
            debugger;
            CoreService.loadingStart("updateRole");
            vm.disableSubmitButton = true;
            vm.rolesData = {"role": vm.role};
            RolesService.updateRole(vm.rolesData, roleId).then(function (res) {
                debugger;
                var title = 'Role Updated Successfully';
                cancel();
                CoreService.loadingStop();
                CoreService.notification('success', 'Role update', title)
            }, function (error) {
                debugger;
                CoreService.loadingStop();
                var title = 'Error While Updating the roles';
                CoreService.notification('error', 'Role update', title)
            });
        }

        function deleteRole() {

        }

    }

})();


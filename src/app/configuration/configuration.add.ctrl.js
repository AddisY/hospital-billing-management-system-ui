/**
 * Created by JIMMY on 3/2/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configuration')
        .controller('ConfigurationAddCtrl', ConfigurationAddCtrl);
    /*@ngNoInject*/
    function ConfigurationAddCtrl($scope, $state, $rootScope, $stateParams, ConfigurationService, CoreService) {
        var vm = this;
        var loading = "";
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "Configuration Add";
        vm.types = {};
        vm.typeData = {};
        vm.postItemType = postItemType;
        vm.getItemDetail = getItemDetail;
        vm.putItemType = putItemType;
        vm.cancel = cancel;
        vm.loadingStart = loadingStart;
        vm.loadingStop = loadingStop;

        if ($stateParams.itemId) {
            vm.update = true;
            getItemDetail($stateParams.itemId);
        }
        function loadingStart(btnName){
            loading = Ladda.create(document.querySelector('.'+btnName));
            loading.start();
        }
        function loadingStop(){
            loading.stop();
        }

        function postItemType() {
            debugger;
            loadingStart("addType");
            vm.disableSubmitButton = true;
            var data = {"type": vm.types};
            ConfigurationService.postItemType(data).then(function (res) {
                cancel();
                loadingStop();
                CoreService.notification('success', "Item Type Create", "Item Type created Successfully");
            }, function (error) {
                debugger;
                loadingStop();
                var title = 'Error While Creating Item Type';
                CoreService.notification('error', 'Creating Item Type ', title)
            });

        }

        function cancel() {
            debugger;
            loadingStart("Cancel");
            $state.go('main.configuration.list');
            loadingStop();
        }

        function getItemDetail(itemId) {
            debugger;
            ConfigurationService.getItemDetail(itemId).then(function (res) {
                debugger;
                vm.types = {
                    // "id": res.Item.id,
                    "name": res.Item.name,
                    "type": res.Item.type
                }
            });

        }

        function putItemType() {
            debugger;
            loadingStart("updateType");
            vm.disableSubmitButton = true;
            ConfigurationService.putItemType({"type": vm.types}, $stateParams.itemId).then(function (res) {
                debugger;
                $state.go('main.configuration.list');
                loadingStop();
                CoreService.notification('success', "Type Updated", "Type updated Successfully");
            }, function (error) {
                debugger;
                loadingStop();
                var title = 'Error While updating  Type';
                CoreService.notification('error', 'Type Update', title)
            });
        }

    }
})
();

/**
 * Created by JIMMY on 3/2/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configuration')
        .controller('ConfigurationCtrl', ConfigurationCtrl);
    /*@ngNoInject*/
    function ConfigurationCtrl($scope, $state, $rootScope) {
        var vm = this;
        $rootScope.currentState = $state.current.name;

        $rootScope.stateName = "Configuration";

    }
})();

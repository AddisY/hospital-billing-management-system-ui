/**
 * Created by JIMMY on 3/2/2017.
 */
(function () {
    'use strict'
    angular
        .module('app.configuration')
        .service('ConfigurationService', ConfigurationService);
    function ConfigurationService(TokenRestangular) {
        var service = {
            getItemTypeList: getItemTypeList,
            postItemType: postItemType,
            deleteItem: deleteItem,
            getItemDetail: getItemDetail,
            putItemType: putItemType

        };
        return service;
        function getItemTypeList(currentPage,filter) {
            debugger;
            return TokenRestangular.all('all_type?page='+currentPage+ filter).customGET('');
        }

        function postItemType(typeData) {
            debugger;
            return TokenRestangular.all('type').customPOST(typeData);
        }
        function deleteItem(itemId) {
            return TokenRestangular.all('type/'+itemId).customDELETE('');
        }
        function getItemDetail(itemId) {
            debugger;
            return TokenRestangular.all('type/'+itemId).customGET('');
        }
        function putItemType(typeData,itemId) {
            debugger;
            return TokenRestangular.all('type/'+itemId).customPUT(typeData);
        }

    }
})();


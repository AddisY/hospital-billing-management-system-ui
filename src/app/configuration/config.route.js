/**
 * Created by JIMMY on 3/2/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configuration')
        .config(moduleConfig);
    /*@ngNoInject*/
    function moduleConfig($stateProvider) {

        $stateProvider
            .state('main.configuration', {
                url: "/configuration",
                templateUrl: "app/configuration/configuration.html",
                controller: 'ConfigurationCtrl',
                controllerAs: 'vm'
            })
            .state('main.configuration.list', {
                url: "/list",
                templateUrl: "app/configuration/configuration.list.html",
                controller: 'ItemTypeListCtrl',
                controllerAs: 'vm'
            })
            .state('main.configuration.add',{
                url:"/add",
                templateUrl:"app/configuration/configuration.add.html",
                controller:'ConfigurationAddCtrl',
                controllerAs:'vm'
            })
            .state('main.configuration.update',{
                url:"/update/{itemId}",
                templateUrl:"app/configuration/configuration.add.html",
                controller:'ConfigurationAddCtrl',
                controllerAs:'vm'
            })
    }

})();



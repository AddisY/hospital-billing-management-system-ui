/**
 * Created by JIMMY on 3/2/2017.
 */
(function () {
    'use strict';

    angular
        .module('app.configuration')
        .controller('ItemTypeListCtrl', ItemTypeListCtrl);
    /*@ngNoInject*/
    function ItemTypeListCtrl($scope, $state, $rootScope, CoreService, ConfigurationService) {
        var vm = this;
        $rootScope.currentState = $state.current.name;
        $rootScope.stateName = "configuration List";
        vm.types = [];
        vm.itemsPerPage = 12;
        vm.totalItems = 0;
        vm.currentPage = 1;
        vm.getItemTypeList = getItemTypeList;
        vm.deleteItem = deleteItem;
        vm.SearchType = SearchType;

        getItemTypeList();


        function getItemTypeList() {
            debugger;
            vm.types = [];
            ConfigurationService.getItemTypeList(vm.currentPage, vm.filter).then(function (res) {
                debugger;
                vm.types = res.data;
                vm.totalItems = res.meta.pagination.total;
                vm.itemsPerPage = res.meta.pagination.per_page;
            }, function (error) {
                debugger;
                var title = 'Error While Listing configuration';
                CoreService.notification('error', 'Configuration Listing', title)
            });

        }
        function SearchType(filter){
            debugger;
            vm.currentPage = 1;
            vm.filter = filter;
            getItemTypeList();
        }

        function deleteItem(itemId){
            debugger;
            $rootScope.cancel();
            ConfigurationService.deleteItem(itemId).then(function () {
                $state.reload('main.configuration.list');
                CoreService.notification('success', "Item deleted", "Item deleted Successfully");
            }, function (error) {
                debugger;
                var title = 'Error While deleting the Item';
                CoreService.notification('error', 'Item Delete', title)
            });
        }

    }

})
();
